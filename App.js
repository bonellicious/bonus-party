/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View} from 'react-native';
import styles from './assets/styles/main';

const fontRegular = Platform.select({
  ios: "Poppins-Regular",
  android: "Poppins Regular"
});

type Props = {};
export default class App extends Component<Props> {
  render() {
    return (
      <View>
        <Text>Welcome to Reeeeact Native!</Text>
        <Text>To get started, eddddit App.js</Text>
      </View>
    );
  }
}
