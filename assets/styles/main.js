import {Platform, StyleSheet} from 'react-native';

const fontRegular = Platform.select({
  ios: "Poppins-Regular",
  android: "Poppins Regular"
});

const styles = StyleSheet.create({
  regular : {
    fontFamily : fontRegular,
  },
  bold : {
    fontFamily : fontRegular,
    fontWeight : "500"
  },
  black : {
    fontFamily : "Poppins-Black",
  },
  app : {
    fontFamily : fontRegular,
  },
  grey : {
    color: "#99AABB"
  },
  greyB : {
    backgroundColor: "#99AABB"
  },
  yellow : {
    color: "#FFD000"
  },
  yellowB : {
    backgroundColor: "#FFD000"
  },
  dark : {
    color: "#334455"
  },
  darkB : {
    backgroundColor: "#334455"
  },
  light : {
    color: "#ddeeff"
  },
  lightB : {
    backgroundColor: "#ddeeff"
  },
});

export default styles;